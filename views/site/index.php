<?php
use yii\helpers\Html;
use app\components\Foto;
/* @var $this yii\web\View */

$this->title = 'Página de inicio';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Hoja de ejercicios 2</h1>

        <p class="lead">Esta es la página de inicio.</p>

        
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4 text-center">
                <h2>Izquierda</h2>

                <?= Foto::widget(["nombre"=>"nada.jpg","alternativo"=>"Foto adjunta"]); ?>
            </div>
            <div class="col-lg-4 text-center">
                <h2>Centro</h2>
                <?= Foto::widget(["nombre"=>"nada.jpg","alternativo"=>"Foto adjunta"]); ?>
                
            </div>
            <div class="col-lg-4">
                <h2>Derecha</h2>
                <?= Foto::widget(["nombre"=>"alpe.png","alternativo"=>"Foto adjunta"]); ?>
                
            </div>
        </div>

    </div>
</div>
